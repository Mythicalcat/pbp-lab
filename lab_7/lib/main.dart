// ignore_for_file: deprecated_member_use, prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'dart:html';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController nama = TextEditingController();

  int index = 0;
  List<String> question = [
    "Halo, silahkan masukan nama anda dibawah ini",
    "Apakah indera penciuman atau perasa anda pernah hilang?",
    "Apakah anda pernah kontak dengan orang yang terkonfirmasi kasus COVID?",
    "Apakah saturasi oksigen anda kurang dari 92%?",
    "Apakah anda pernah mengalami sesak napas dalam beberapa hari terakhir?"
  ];

  List<Widget> options = [];

  setup() {
    options = [
      Container(
        margin: EdgeInsets.all(20),
        child: TextField(
          controller: nama,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            focusedBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
            enabledBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
            labelText: 'Nama',
            labelStyle: TextStyle(color: Colors.white),
          ),
        ),
      ),
    ];
  }

  Color col = Color(0xFF181818);
  Color acc = Color(0xFF818CF8);

  @override
  Widget build(BuildContext context) {
    setup();
    return MaterialApp(
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: col,
          shadowColor: acc,
          title: Text("Covid 19 Assessment Test"),
        ),
        body: Center(
          child: Container(
            color: col,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20),
                  child: Text(
                    question[index],
                    style: TextStyle(
                      fontSize: 50,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  child: options[0],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      child: FlatButton(
                        color: acc,
                        child: Text(
                          "Ya",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          setState(() {
                            if (index < 3) {
                              index++;
                            }
                          });
                        },
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: FlatButton(
                        color: acc,
                        child: Text(
                          "Tidak",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          setState(() {
                            if (index > 0) {
                              index--;
                            }
                          });
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
