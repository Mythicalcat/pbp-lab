from django.urls import path
from .views import index
from .views import friend_form


urlpatterns = [
    path('', index, name='index'),
    path('add', friend_form, name='friend_form'),
]
